# n-queens

A scheme program that returns the number of ways that n queens can be positioned on an nxn chess board.

# usage

The n-queens function will take n as an argument and return the number of configurations.
