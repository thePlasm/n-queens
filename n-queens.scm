(define
	(find-x x queens)
	(if
		(null? queens)
		#f
		(if
			(= x (car (car queens)))
			#t
			(find-x x (cdr queens)))))

(define
	(check-x queens acc)
	(if
		(null? queens)
		acc
		(check-x
			(cdr queens)
			(and
				acc
				(not (find-x (car (car queens)) (cdr queens)))))))

(define
	(find-y y queens)
	(if
		(null? queens)
		#f
		(if
			(= y (cdr (car queens)))
			#t
			(find-y y (cdr queens)))))

(define
	(check-y queens acc)
	(if
		(null? queens)
		acc
		(check-y
			(cdr queens)
			(and
				acc
				(not (find-y (cdr (car queens)) (cdr queens)))))))

(define
	(find-diags queen queens)
	(if
		(null? queens)
		#f
		(if
			(=
				(abs (- (car queen) (car (car queens))))
				(abs (- (cdr queen) (cdr (car queens)))))
			#t
			(find-diags queen (cdr queens)))))

(define
	(check-diags queens acc)
	(if
		(null? queens)
		acc
		(check-diags
			(cdr queens)
			(and
				acc
				(not (find-diags (car queens) (cdr queens)))))))

(define
	(check-config queens)
	(if
		(null? queens)
		#t
		(and
			(check-x queens #t)
			(check-y queens #t)
			(check-diags queens #t))))

(define
	(n-queens-recurser n curr queens acc)
	(if
		(= curr 0)
		acc
		(n-queens-recurser
			n
			(- curr 1)
			queens
			(if
				(not (or (find-y (- curr 1) queens) (find-diags (cons (length queens) (- curr 1)) queens)))
				(+ acc (n-queens-helper n (cons (cons (length queens) (- curr 1)) queens)))
				acc))))

(define
	(n-queens-helper n queens)
	(if
		(< (length queens) n)
		(n-queens-recurser n n queens 0)
		(begin
			(display queens)
			(display #\newline)
			1)))

(define
	(n-queens n)
	(n-queens-recurser n n '() 0))